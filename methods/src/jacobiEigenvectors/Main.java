package jacobiEigenvectors;

import matix.Matrix;

/**
 * Created by olessavluk on 11/19/14.
 */
public class Main {
    public static void main(String[] agrs) {
        Matrix m = new Matrix(3, Matrix.FILL_SYMMETRIC);

        System.out.println("==========Original===================");
        m.outB = false;
        m.solved =false;
        System.out.println(m.toString());

        Solver s = new Solver();
        s.Jacobi(m);
    }
}
