package jacobiEigenvectors;

import matix.Matrix;
import searchRanking.Pair;

/**
 * Created by olessavluk on 11/19/14.
 */
public class Solver {
    static final double eps = 1e-6;
    static final double jeps = 1e-3;

    public Matrix Jacobi(Matrix matrix) {
        Matrix u = new Matrix(matrix.size);
        u.fillE();

        double norm = 0;
        do {
//            System.out.println("==========Matrix===================");
//            matrix.solved = false;
//            matrix.outB = false;
//            System.out.println(matrix.toString());
            double max = -1;
            int x = -1;
            int y = -1;

            for (int i = 0; i < matrix.size; i++) {
                for (int j = 0; j < matrix.size; j++) {
                    if (i != j) {
                        if (x == -1 || Math.abs(matrix.A[i][j]) > max) {
                            max = Math.abs(matrix.A[i][j]);
                            x = i;
                            y = j;
                        }
                    }
                }
            }

            double f = Math.PI / 4;
            if (Math.abs(matrix.A[x][x] - matrix.A[y][y]) > eps) {
                f = 0.5 * Math.atan(2 * matrix.A[x][y] / (matrix.A[x][x] - matrix.A[y][y]));
            }

            Matrix uk = new Matrix(matrix.size);
            uk.fillE();
            uk.A[x][x] =  Math.cos(f);
            uk.A[y][y] = Math.cos(f);
            uk.A[x][y] = -Math.sin(f);
            uk.A[y][x] = Math.sin(f);

            u = u.multiply(uk);

            matrix = matrix.rotate(uk);

            norm = 0;
            for (int i = 0; i < matrix.size; i++) {
                for (int j = 0; j < matrix.size; j++) {
                    if (i != j)
                        norm += Math.pow(matrix.A[i][j], 2.0);
                }
            }
        } while (norm > jeps);

        System.out.println("==========Eigenvectors===================");
        u.solved = false;
        u.outB = false;
        System.out.println(u.toString());

        System.out.println("==========Eigenvalues===================");
        matrix.solved = false;
        matrix.outB = false;
        for (int i = 0; i < matrix.size; i++) {
            for (int j = 0; j < matrix.size; j++) {
                if (i != j)
                    matrix.A[i][j] = 0;
            }
        }
        System.out.println(matrix.toString());
        return matrix;
    }
}
