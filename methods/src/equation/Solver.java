package equation;

import java.util.NoSuchElementException;

/**
 * Created by Olesuk on 9/10/14.
 */
public class Solver {
    static double eps = 1.0e-10;

    public double func(double x) {
        return x * x - 25;
    }

    public double funcDerivative(double x) {
        return 2 * x;
    }

    public double phi(double x) {
        //such that |phi'(x)|<1 xe[a,b]
        double alpha = -0.1;
        return x + alpha * func(x);
    }

    public double dichotomy(double a, double b) {
        double c;

        if (!(b > a && func(a) < 0 && func(b) > 0)) {
            throw new NoSuchElementException("bad a and b");
        }

        while (b - a > eps) {
            c = (b + a) / 2;
            if (func(a) * func(c) <= 0) {
                b = c;
            } else {
                a = c;
            }
        }

        return (b + a) / 2;
    }

    public double newtons(double x) {
        double xi = x;

        do {
            if (funcDerivative(x) == 0)
                throw new NoSuchElementException("division by zero");

            x = xi;
            xi = x - func(x) / funcDerivative(x);
        } while (Math.abs(xi - x) > eps);

        return xi;
    }

    public double relaxation(double x) {
        double xi = x;
        do {
            x = xi;
            xi = phi(x);
        } while (Math.abs(xi - x) > eps);

        return x;
    }
}

