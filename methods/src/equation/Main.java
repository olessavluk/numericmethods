package equation;

import java.text.DecimalFormat;

/**
 * Created by olessavluk on 9/10/14.
 */
public class Main {
    public static void main(String[] args) {
        Solver solver = new Solver();
        double dichotomy = solver.dichotomy(0, 10);
        double newton = solver.newtons(1);
        double relaxation = solver.relaxation(1);

        DecimalFormat df = new DecimalFormat("#####.######");
        System.out.println("Dichotomy: " + df.format(dichotomy));
        System.out.println("Newton's : " + df.format(newton));
        System.out.println("Relaxation : " + df.format(relaxation));
    }
}
