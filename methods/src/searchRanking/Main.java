package searchRanking;

import matix.Matrix;

/**
 * Created by Olesuk on 9/10/14.
 */
public class Main {
    public static void main(String[] args) {
        int n = 4;
        double M = 0.15;
        int[] step = new int[n];
        //edge pairs
        Pair[] p = {new Pair(1, 3), new Pair(3, 1), new Pair(1, 4), new Pair(4, 1), new Pair(1, 2), new Pair(2, 3), new Pair(2, 4), new Pair(4, 3)};
        for(int i=0; i<p.length; i++) {
            step[p[i].a - 1]++;
        }

        Matrix m = new Matrix(n, Matrix.FILL_RAND, 0, 0);
        m.solved = false;
        m.outB = false;

        for(int i=0; i<p.length; i++) {
            m.A[p[i].b-1][p[i].a-1] = (double) 1/step[p[i].a-1];
        }

        System.out.print(m.toString());

        for (int i = 0; i < m.size; i++) {
            for (int j = 0; j < m.size; j++) {
                m.A[i][j] *= (1-M);
                m.A[i][j] += M / n;
            }
        }

        System.out.print(m.toString());

        double[] x = new double[n];
        double eps = 1e-6;
        double myEps;

        for (int i = 0; i < m.size; i++) {
            m.x[i] = (double) 1/n;
            x[i] = 0;
        }

        int iterations = 0;
        do {
            for (int i = 0; i < m.size; i++) {
                double sum = 0;
                for (int j = 0; j < m.size; j++) {
                    sum += m.A[i][j] * m.x[j];
                }
                x[i] = sum;
            }

            myEps = 0;
            for (int i = 0; i < m.size; i++) {
                myEps += Math.pow(m.x[i] - x[i], 2.0);
                m.x[i] = x[i];
            }
            iterations++;
        } while (myEps > eps);

        m.solved = true;
        System.out.print(m.toString());
        System.out.println("Iterations: " + iterations + "\n");
    }
}
