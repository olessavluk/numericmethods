package searchRanking;

/**
 * Created by olessavluk on 10/22/14.
 */
public class Pair {
    int a, b;

    Pair(int a, int b) {
        this.a = a;
        this.b = b;
    }
}
